%{
/********
HM of Tianhe2
created on 2022.10.10
*************/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#ifndef YYSTYPE
#define YYSTYPE char*
#endif
int yylex();
int flag=0;
char idStr[50];
char numStr[50];
extern int yyparse();
FILE* yyin;
void yyerror(const char* s );
%}

%token NUMBER
%token ID
%token ADD SUB MUL DIV  
%token LB RB
%left ADD SUB 
%left MUL DIV
%left RB
%right LB 
%right UMINUS



%%
lines   :   lines expr ';'   {printf("%s\n",$2);}
        |   lines ';'
        |
        ;

expr    :   expr ADD expr   {$$ = (char*)malloc(50*sizeof (char));strcpy($$,$1); strcat($$,$3); strcat($$,"+ ");}
        |   expr SUB expr   {$$=(char*)malloc(50*sizeof(char));strcpy($$,$1);strcat($$,$3);strcat($$,"- ");}
        |   expr MUL expr   {$$=(char*)malloc(50*sizeof(char));strcpy($$,$1);strcat($$,$3);strcat($$,"* ");}
        |   expr DIV expr   {$$=(char*)malloc(50*sizeof(char));strcpy($$,$1);strcat($$,$3);strcat($$,"/ ");}
        |   LB expr RB      {$$=(char*)malloc(50*sizeof(char));strcpy($$,$2);strcat($$," ");}
        |   UMINUS expr     {$$=(char*)malloc(50*sizeof(char));strcat($$,"-");strcat($$,$2);} 
        |   NUMBER          {$$=(char*)malloc(50*sizeof(char));strcpy($$,$1);strcat($$," ");}
        |   ID              {$$=(char*)malloc(50*sizeof(char));strcpy($$,$1);strcat($$," ");}
        ;



%%

// programs section
int yylex()
{
   int t;
   while(1){
    t=getchar();
    if(t==' '||t=='\t'||t=='\n'){
        continue;  
    }
    else if(t>='0'&&t<='9'){
        flag=1;
        int ti=0;
        while(t>='0'&&t<='9')
        {
            numStr[ti]=t;
            t=getchar();
            ti++;
        }
        numStr[ti]='\0';
        yylval=numStr;
        //printf("%s",yylval);
        ungetc(t,stdin);
        return NUMBER;
    }
    else if(t>='a'&&t<='z'||t>='A'&&t<='Z'||t=='_'){
        flag=1;
        int ti=0;
        while(t>='a'&&t<='z'||t>='A'&&t<='Z'||t=='_'||isdigit(t)){
            idStr[ti]=t;
            t=getchar();
            ti++;
        }
        idStr[ti]='\0';
        yylval=idStr;
        ungetc(t,stdin);
        return ID;

    }

    
    else if(t=='+'){
        flag=0;
        /* printf("%s",t); */
        return ADD;
    }
    
    else if(t=='-'){
        if(flag){
            flag=0;
            return SUB;
        }
        else
            return UMINUS;
    }
    else if(t=='*'){
        flag=0;
        return MUL;
    }
    else if(t=='/'){
        flag=0;
        return DIV;
    }
    else if(t=='('){
        flag=0;
        return LB;
    }
    else if(t==')'){
        flag=1;
        return RB;
    }
    flag=0;
    return t;

   }
} 
int main(void){
    yyin=stdin;
    do{
        yyparse();
    }while(!feof(yyin));
    return 0;
}
void yyerror(const char* s){
    fprintf(stderr,"Parse error:%s\n",s);
    exit(1);
}



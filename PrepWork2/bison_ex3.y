%{
/***********************************************************************
HM of Tianhe2
created on 2022.10.11
***********************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#ifndef YYSTYPE
#define YYSTYPE char*
#endif
char idStr [50];
char numStr[50];
int yylex ();
extern int yyparse();
FILE* yyin ;
void yyerror(const char* s );
int isSub=0;  //判断‘-’是否是减号
%}

%token NUMBER
%token ID
%left ADD SUB
%left MUL DIV
%left RB  //右括号
%right LB  //左括号
%right UMINUS

%%
lines : lines expr ';' {printf("%s\n",$2);} 
      | lines ';'
      |
      ;

expr  : expr ADD expr  { $$ = (char*)malloc(50*sizeof (char)); strcpy($$,$1); strcat($$,$3); strcat($$,"+ "); }
      | expr SUB expr  { $$ = (char*)malloc(50*sizeof (char)); strcpy($$,$1); strcat($$,$3); strcat($$,"- "); }
      | expr MUL expr  { $$ = (char*)malloc(50*sizeof (char)); strcpy($$,$1); strcat($$,$3); strcat($$,"* "); }
      | expr DIV expr  { $$ = (char*)malloc(50*sizeof (char)); strcpy($$,$1); strcat($$,$3); strcat($$,"/ "); }
      | NUMBER         { $$ = (char*)malloc(50*sizeof (char)); strcpy($$,$1); strcat($$," ");} 
      | ID             { $$ = (char*)malloc(50*sizeof (char)); strcpy($$,$1); strcat($$," ");} 
      | LB expr RB     { $$ = (char*)malloc(50*sizeof (char)); strcpy($$,$2); strcat($$," ");} 
      | UMINUS expr    { $$ = (char*)malloc(50*sizeof (char)); strcat($$,"-"); strcat($$,$2);} 
      ;
%%


// programs section
int yylex()
{
    // place your token retrieving code here
    int t ;
    while (1) 
    {
        t = getchar ();
        if(t==' ' || t=='\t' || t=='\n')
        {
            //do nothing
            continue;
        }
        if(isdigit(t))
        {
            isSub=1;
            // "The semantic value of the token (if it has one) is stored into the global variable yylval, which is where the Bison parser will look for it."
            yylval = 0;
            int ti=0;
            while (isdigit(t))
            {
                numStr[ti]=t;
                t = getchar();
                ti++;
            }
            numStr[ti]='\0';
            yylval=numStr;
            ungetc(t , stdin);
            return NUMBER;
        }
        if((t>='a'&&t<='z') || (t>='A'&&t<='Z') || (t=='_'))
        {
            int ti=0;
            while((t>='a'&&t<='z') || (t>='A'&&t<='Z') || (t=='_') || (t>='0'&&t<='9'))
            {
                idStr[ti]=t;
                ti++;
                t=getchar();
            }
            idStr[ti]='\0';
            yylval=idStr;
            ungetc(t , stdin);
            return ID;
        }

        if(t=='+') {isSub=0; return ADD;}
        if(t=='*') {isSub=0; return MUL;}
        if(t=='/') {isSub=0; return DIV;}
        if(t=='(') {isSub=0; return LB;}
        if(t==')') {isSub=1; return RB;}
        if(t=='-')
        {
            //printf("isSub:%d\n",isSub);
            if(isSub) {isSub=0; return SUB;}
            else {isSub=0; return UMINUS;}

        } 
        isSub=0; return t;
    }
}

int main(void)
{
    yyin=stdin;
    do{
        yyparse();
    }while(!(feof(yyin)));
    return 0;
}

void yyerror(const char* s) 
{
    fprintf (stderr , "Parse error : %s\n", s );
    exit (1);
}

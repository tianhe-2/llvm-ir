%{
/********
HM of Tianhe2
created on 2022.10.15
*************/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

int yylex();
// int isdigit(char c);
int flag=0;
char* last_variable;
int isSub=0;
char idStr[50];
char numStr[50];
extern int yyparse();
FILE* yyin;
void yyerror(const char* s );
struct table{
    char* name;
    double value;    
};
//存储符号表的长度
int table_num=0;
struct table sym_table[100];
%}
%union{
    char* id;
    double val;
}

%token NUMBER
%token ID
%token EQUAL
/* %token ADD SUB MUL DIV   */
%token LB RB
%left ADD SUB 
%left MUL DIV
%left RB
%right LB 
%right UMINUS



%%  

lines   :   lines expr ';'   {printf("%f\n",$<val>2);}
        |   lines ';'
        |   lines ID EQUAL expr ';' {  
                                // printf("num%f\n",$<val>4);
                                // 符号表里一定有，返回符号表的值
                                for(int i=0;i<table_num;i++)
                                { 
                                    // printf("come in id lines ");
                                    if(!strcmp($<id>2,sym_table[i].name)){
                                        sym_table[i].value=$<val>4;
                                        // printf("sym_value[%d]%f\n",i,sym_table[i].value);
                                        break;
                                    }
                                }
                            }
        |
        ;

expr    :   expr ADD expr   {$<val>$=$<val>1+$<val>3;}
        |   expr SUB expr   {$<val>$=$<val>1-$<val>3;}
        |   expr MUL expr   {$<val>$=$<val>1*$<val>3;}
        |   expr DIV expr   {$<val>$=$<val>1/$<val>3;}
        |   LB expr RB      {$<val>$=$<val>2;}
        |   UMINUS expr     {$<val>$=-$<val>2;} 
        |   NUMBER          {$<val>$=$<val>1;}
        |   ID              {
                                int isintable=0;
                                double idvalue;
                                // 符号表里一定有，返回符号表的值
                                for(int i=0;i<table_num;i++)
                                { 
                                    // printf("come in id\n");
                                    if(!strcmp($<id>1,sym_table[i].name)){
                                        $<val>$=sym_table[i].value;
                                        break;
                                    }
                                    // printf("$<val>$ %f\n",$<val>$);
                                }
                                // printf("xunhuanend sym_table[%d].name %s,sym_table[%d].value %f\n",table_num-1,sym_table[table_num-1].name,table_num-1,sym_table[table_num].value);
            }
        

                                
                            
        ;



%%

// programs section
int yylex()
{
   int t;
   while(1){
    t=getchar();
    if(t==' '||t=='\t'||t=='\n'){
        continue;  
    }
    else if(t>='0'&&t<='9'){
        flag=1;
        int ti=0;
        /* 这是个全局变量，需要每次赋0 */
        yylval.val=0;
        while(t>='0'&&t<='9')
        {
            /* numStr[ti]=t;
            t=getchar();
            ti++; */
            yylval.val=yylval.val*10+t-'0';
            
            t=getchar();
        }
        /* numStr[ti]='\0'; */
        /* printf("yylval.val%f   ",yylval.val); */
        /* yylval.id=numStr; */
        //printf("%s",yylval);
        ungetc(t,stdin);
        return NUMBER;
    }
    else if(t>='a'&&t<='z'||t>='A'&&t<='Z'||t=='_'){

        flag=1;
        int ti=0;
        while(t>='a'&&t<='z'||t>='A'&&t<='Z'||t=='_'||isdigit(t)){
            idStr[ti]=t;
            t=getchar();
            ti++;
        }
        
        idStr[ti]='\0';
        /* 指针型，需要给他分配空间 */
        yylval.id=(char*)malloc(50*sizeof(char));
        strcpy(yylval.id,idStr);
        /* printf("yylval.id %s\n",yylval.id); */
    
        
        int isintable=0;
        //变量放入符号表
        for(int i=0;i<table_num;i++){
            /* printf("%s\n",sym_table[i].name); */
            /* printf("nowname%s\n",idStr); */
            if(!strcmp(sym_table[i].name,idStr)){
                isintable=1;
            }
        }
        if(!isintable){
            sym_table[table_num].name=(char*)malloc(50*sizeof(char));
            strcpy(sym_table[table_num].name,idStr);
            sym_table[table_num].value=0;
            table_num++;

        }
        /* printf("table_num %d \n",table_num);
        printf("whether in %d \n",isintable);
        printf("sym_table[%d].name %s,sym_table[%d].value %f\n",table_num-1,sym_table[table_num-1].name,table_num-1,sym_table[table_num].value); */
        ungetc(t,stdin);
        return ID;

    }
    else if(t=='='){
        flag=0;
        return EQUAL;

    }

    
    else if(t=='+'){
        flag=0;
        return ADD;
    }
    
    else if(t=='-'){
        if(flag){
            flag=0;
            return SUB;
        }
        else
            return UMINUS;
    }
    else if(t=='*'){
        flag=0;
        return MUL;
    }
    else if(t=='/'){
        flag=0;
        return DIV;
    }
    else if(t=='('){
        flag=0;
        return LB;
    }
    else if(t==')'){
        flag=1;
        return RB;
    }
    flag=0;
    return t;

   }
} 
int main(void){
    yyin=stdin;
    do{
        yyparse();
    }while(!feof(yyin));
    return 0;
}
void yyerror(const char* s){
    fprintf(stderr,"Parse error:%s\n",s);
    exit(1);
}



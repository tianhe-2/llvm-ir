//定义全局变量
    .global a
    .global b
a:
        .word   3
b:
        .word   5       //int a=3,b=4全局变量
		.global reverse
//reverse函数
reverse:
        push    {fp, lr}		//栈底指针，返回地址压栈，
        add     fp, sp, #4		//修改栈帧
        sub     sp, sp, #16		//修改栈顶指针
        str     r0, [fp, #-16]	//传参数n
        ldr     r3, [fp, #-16]	
        cmp     r3, #1
        bgt     .greater_than
        bl      getint      	//调函数
        str     r0, [fp, #-8]	//得到next 赋值
        ldr     r0, [fp, #-8]
        bl      putint			//跳转到putint 打印
        b       .end
.greater_than:
        bl      getint			
        str     r0, [fp, #-8]
        ldr     r3, [fp, #-16]	//r3 把n取出来
        sub     r3, r3, #1		//n-1
        mov     r0, r3			//把新的参数写在r0
        bl      reverse			//调用reverse r0传参
        ldr     r0, [fp, #-8]	//next 传到r0
        bl      putint			//打印
.end:
        nop
        sub     sp, fp, #4		//调整栈帧
        pop     {fp, lr}
        bx      lr
		.global main

//main函数
		.global main
main:
        //井号代表立即数
        //fp：栈底指针寄存器
        //sp：栈顶指针寄存器
        //LR:In AArch64 state, the Link Register (LR) stores the return address when a subroutine call is made. 
        //   It can also be used as a general-purpose register if the return address is stored on the stack.
        push    {fp, lr}
        add     fp, sp, #4    
        sub     sp, sp, #96 


        //处理同名全局变量、局部变量赋值部分
        ldr     r3, .LGlobalDef
        ldr     r2, [r3]   //目前r2里存储的是global a
        ldr     r3, .LGlobalDef+4
        ldr     r3, [r3]   //目前r3里存储的是global b的值
        add     r3, r2, r3 
        str     r3, [fp, #-16] //fp-16存储local c
        ldr     r0, [fp, #-16]  
        bl      putint  
        mov     r3, #5    //目前r3里存储的是local a的值
        str     r3, [fp, #-20] //fp-20存储local a
        ldr     r3, .LGlobalDef+4 
        ldr     r3, [r3] //目前r3里存储的是local b的值
        ldr     r2, [fp, #-20] 
        add     r3, r2, r3 
        str     r3, [fp, #-24] //fp-24存储local d的值
        ldr     r0, [fp, #-24]
        bl      putint


        //数组初始化
        sub     r3, fp, #68   
        mov     r2, #32
        mov     r1, #0
        mov     r0, r3
        bl      memset
        sub     r3, fp, #100
        mov     r2, #32
        mov     r1, #0
        mov     r0, r3
        bl      memset

        //处理隐式类型转换部分
        mov     r3, #6          //隐式类型转换，f值6.5->6
        str     r3, [fp, #-28]  //fp-28存储local f的值
        ldr     r0, [fp, #-28]
        bl      putint


        //if语句
        ldr     r3, [fp, #-20]
        cmp     r3, #0
        bge     .LI1_L1_F
        mov     r3, #1
        b       .LMainReturn
//第一个if，第一层，不满足（a大于等于1）
.LI1_L1_F: 
        ldr     r3, [fp, #-20]
        cmp     r3, #5
        bne     .LI2_L1_F
        ldr     r3, .LGlobalDef+4
        ldr     r3, [r3]
        cmp     r3, #10
        bne     .LI2_L2_F
        mov     r3, #25
        str     r3, [fp, #-20]
        b       .LI2_L2_F
//第二个if，第二层，不满足（a不等于5）
.LI2_L1_F:
        ldr     r3, [fp, #-20]
        add     r3, r3, #15
        str     r3, [fp, #-20]
//第二个if，第二层，不满足（b不等于10）


        //while循环
.LI2_L2_F:
        mov     r3, #3
        str     r3, [fp, #-8]  //fp-8存储local i的值
        b       .LWhile
//while循环体里执行的动作
.LWhileAction:
        ldr     r3, [fp, #-8]
        add     r3, r3, #30
        str     r3, [fp, #-8]
//while循环
.LWhile:
        ldr     r3, [fp, #-8]
        cmp     r3, #99
        ble     .LWhileAction
        ldr     r0, [fp, #-8]
        bl      putint


        //for循环
        mov     r3, #1
        str     r3, [fp, #-12]   //fp-8存储for i的值
        b       .LFor
//for循环体里执行的动作
.LForAction:
        ldr     r0, [fp, #-12]
        bl      putint
        ldr     r3, [fp, #-12]
        add     r3, r3, #1
        str     r3, [fp, #-12]
.LFor:
        ldr     r3, [fp, #-12]
        cmp     r3, #4
        ble     .LForAction
        mov     r3, #6      //int j=6 r3存j
        str     r3, [fp, #-32]      
        ldr     r0, [fp, #-32]      //把j写在r0寄存器里，为下一步函数调用做准备
        bl      reverse
        mov     r3, #0          //r3寄存器置0
.LMainReturn:
        mov     r0, r3
        sub     sp, fp, #4
        pop     {fp, lr}
        bx      lr
.LGlobalDef:
        .word   a
        .word   b
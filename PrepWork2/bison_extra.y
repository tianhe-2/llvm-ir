%{
/***********************************************************************
HM of Tianhe2
created on 2022.10.10
***********************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
char idStr [50];
int yylex ();
extern int yyparse();
FILE* yyin ;
void yyerror(const char* s );
int isSub=0;  //判断‘-’是否是减号
struct ID{
    char* name;
    double val;
}table[50];  //符号表
int idCnt=0;
%}

//使YYSTYPE不只能代表一个类型
%union{
    double dtype;
    char* ctype;  
}

//token:Declare a terminal symbol (token kind name) with no precedence or associativity specified
//right:Declare a terminal symbol (token kind name) that is right-associative
// C version:
%token END  
%token NUMBER
%token ID
%token EQUAL
%nonassoc EQUAL
//%left EQUAL
%left ADD SUB
//%right ADD SUB
%left MUL DIV
%left RB  //右括号
%right LB  //左括号
%right UMINUS 

%type <dtype> expr
%type <dtype> lines
//%type <dtype> blocks
%type <dtype> NUMBER



%%
//C version:
//prgm  :  lines expr ';' //{printf("%d\n",$<dtype>2);}  
//      ;

// prgm1 : prgm1 lines
//       | lines
//       ;

// blocks : blocks lines 
//        |
//        ;

lines : lines expr ';'  {printf("%f\n",$<dtype>2);} 
      | lines ';'
      | lines ID EQUAL expr ';'{
                                    //printf("lines: %s\n",$<ctype>1);
                                    printf("ID(2): %s\n",$<ctype>2);
                                    int idxFound=-1;
                                    for(int i=0;i<idCnt;i++)
                                    {
                                        if(strcmp(table[i].name,$<ctype>2)==0)
                                        {
                                            idxFound=i;
                                            break;
                                        }
                                    }
                                    if(idxFound==-1) //没找到
                                    {
                                        table[idCnt].name = (char*)malloc(50*sizeof(char));
                                        strcpy(table[idCnt].name,$<ctype>2);
                                        table[idCnt++].val=$<dtype>4;
                                    }
                                    else //找到了
                                        {table[idxFound].val=$<dtype>4; printf("found lines\n");}
                                }
      |
      ;

expr  : expr ADD expr { $<dtype>$ = $<dtype>1 + $<dtype>3; }
      | expr SUB expr { $<dtype>$ = $<dtype>1 - $<dtype>3; }
      | expr MUL expr { $<dtype>$ = $<dtype>1 * $<dtype>3; }
      | expr DIV expr { $<dtype>$ = $<dtype>1 / $<dtype>3; }
      | LB expr RB { $<dtype>$ = $<dtype>2; }
      | UMINUS expr { $<dtype>$ = -$<dtype>2; }
      | NUMBER { $<dtype>$ = $<dtype>1; }
      | ID  {
                printf("ID in expr:%s\n",$<ctype>1);
                int idxFound=-1;
                printf("idCnt: %d\n",idCnt);
                for(int i=0;i<idCnt;i++)
                {
                    if(strcmp(table[i].name,$<ctype>1)==0)
                    {
                        printf("table[i].name: %s\n",table[i].name);
                        printf("$<ctype>1: %s\n",$<ctype>1);
                        idxFound=i;
                        break;
                    }
                }
                //if(idxFound!=-1){$<dtype>$ = table[idxFound].val; $<ctype>$=$<ctype>1;}
                //else{printf("unfound expr\n");$<dtype>$ =0; $<ctype>$=$<ctype>1;}
                if(idxFound!=-1){$<dtype>$ = table[idxFound].val; }
                else{printf("unfound expr\n");$<dtype>$ =0; }
            }
      ;

%%


// programs section


int yylex()
{
    // place your token retrieving code here
        int t ;
        while (1) 
        {
            t = getchar ();
            if(t==' ' || t=='\t' || t=='\n')
            {
                //do nothing
                continue;
            }
            //if(isdigit(t))
            if(t>='0'&&t<='9')
            {
                isSub=1;
                yylval.dtype = 0;
                //while ( isdigit (t))
                while((t>='0'&&t<='9'))
                {
                    yylval.dtype = yylval.dtype*10 + t-'0';
                    t = getchar ();
                }
                ungetc(t , stdin);
                return NUMBER;
            }
            if((t>='a'&&t<='z') || (t>='A'&&t<='Z') || (t=='_'))
            {
                int ti=0;
                memset(&idStr, 0, 50*sizeof(char));
                while((t>='a'&&t<='z') || (t>='A'&&t<='Z') || (t=='_') || (t>='0'&&t<='9'))
                {
                    idStr[ti]=t;
                    ti++;
                    t=getchar();
                }
                idStr[ti]='\0';
                //strcpy(yylval.ctype,idStr);    segmentation fault!!!!!!!!!!!!!!!!!!!!!
                //yylval.ctype=idStr;    idStr改变，原先的idStr值会丢失
                yylval.ctype=(char*)malloc(50*sizeof(char));
                strcpy(yylval.ctype,idStr);
                ungetc(t , stdin);
                return ID;
            }

            if(t=='+') {isSub=0; return ADD;}
            if(t=='*') {isSub=0; return MUL;}
            if(t=='/') {isSub=0; return DIV;}
            if(t=='(') {isSub=0; return LB;}
            if(t==')') {isSub=1; return RB;}
            if(t=='=') {isSub=0; return EQUAL;}
            if(t=='-')
            {
                if(isSub) {isSub=0; return SUB;}
                else {isSub=0; return UMINUS;}

            } 
            isSub=0; return t;
        }
}

int main(void)
{
    yyin=stdin;
    do{
        yyparse();
    }while(!(feof(yyin)));
    return 0;
}

void yyerror(const char* s) 
{
    fprintf (stderr , "Parse error : %s\n", s );
    exit (1);
}


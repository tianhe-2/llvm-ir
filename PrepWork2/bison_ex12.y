%{
/***********************************************************************
HM of Tianhe2
created on 2022.10.10
***********************************************************************/
#include <stdio.h>
#include <stdlib.h>
#ifndef YYSTYPE
#define YYSTYPE double
#endif
int yylex ();
extern int yyparse();
FILE* yyin ;
void yyerror(const char* s );
int isSub=0;  //判断‘-’是否是减号
%}

%token ADD
%token SUB
%token MUL
%token DIV
%token UMINUS
%token LB //左括号
%token RB //右括号
%token NUMBER

%left ADD SUB
%left MUL DIV
%right RB
%left LB
%right UMINUS

%%
lines : lines expr ';' {printf("%f\n",$2);} 
      | lines ';'
      |
      ;

expr  : expr ADD expr { $$ = $1 + $3; }
      | expr SUB expr { $$ = $1 - $3; }
      | expr MUL expr { $$ = $1 * $3; }
      | expr DIV expr { $$ = $1 / $3; }
      | LB expr RB { $$ = $2; }
      | UMINUS expr { $$ = -$2; }
      | NUMBER { $$ = $1; }
      ;
%%


// programs section
int yylex()
{
    // place your token retrieving code here
    int t ;
    while (1) 
    {
        t = getchar ();
        if(t==' ' || t=='\t' || t=='\n')
        {
            //do nothing
            continue;
        }
        else if(isdigit(t))
        {
            isSub=1;
            //printf("isSub:%d\n",isSub);
            yylval = 0;
            while ( isdigit (t))
            {
                yylval = yylval*10 + t-'0';
                t = getchar ();
            }
            ungetc(t , stdin);
            return NUMBER;
        }
        if(t=='+') {isSub=0; return ADD;}
        if(t=='*') {isSub=0; return MUL;}
        if(t=='/') {isSub=0; return DIV;}
        if(t=='(') {isSub=0; return LB;}
        if(t==')') {isSub=1; return RB;}
        if(t=='-')
        {
            //printf("isSub:%d\n",isSub);
            if(isSub) {isSub=0; return SUB;}
            else {isSub=0; return UMINUS;}

        } 
        isSub=0; return t;
    }
}

int main(void)
{
    yyin=stdin;
    do{
        yyparse();
    }while(!(feof(yyin)));
    return 0;
}

void yyerror(const char* s) 
{
    fprintf (stderr , "Parse error : %s\n", s );
    exit (1);
}

%{
/********
HM of Tianhe2
created on 2022.10.10
*************/
#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#ifndef YYSTYPE
#define YYSTYPE double
#endif
int yylex ();
int flag=0;
extern int yyparse();
FILE* yyin;
void yyerror(const char* s );
%}

%token NUMBER
%token ADD
%token SUB MUL DIV 
%token LB RB
%left ADD SUB 
%left MUL DIV
%left RB
%right LB
%right UMINUS



%%
lines   :   lines expr ';'   {printf("%f\n",$2);}
        |   lines ';'
        |
        ;

expr    :   expr ADD expr   {$$=$1+$3;}
        |   expr SUB expr   {$$=$1-$3;}
        |   expr MUL expr   {$$=$1*$3;}
        |   expr DIV expr   {$$=$1/$3;}
        |   LB expr RB      {$$=$2;}
        |   UMINUS expr     {$$=-$2;} 
        |   NUMBER          {$$=$1;}
        ;



%%

// programs section
int yylex()
{
  
   int t;
   while(1){
    t=getchar();
    if(t==' '||t=='\t'||t=='\n'){
        continue;
        
    }
    else if(isdigit(t)){
        flag=1;
        yylval=0;
        while(isdigit(t))
        {
            yylval=yylval*10+t-'0';
            /* printf("%f",yylval); */
            t=getchar();
        }
        ungetc(t,stdin);
        return NUMBER;
    }
    
    else if(t=='+'){
        flag=0;
        return ADD;
    }
    else if(t=='-'){
        if(flag){
            flag=0;
            return SUB;
        }
        else
            return UMINUS;
    }
    else if(t=='*'){
        flag=0;
        return MUL;
    }
    else if(t=='/'){
        flag=0;
        return DIV;
    }
    else if(t=='('){
        flag=0;
        return LB;
    }
    else if(t==')'){
        flag=1;
        return RB;
    }
    flag=0;
    return t;

   }
} 
int main(void){
    yyin=stdin;
    do{
        yyparse();
    }while(!feof(yyin));
    return 0;
}
void yyerror(const char* s){
    fprintf(stderr,"Parse error:%s\n",s);
    exit(1);
}



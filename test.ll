; ModuleID = 'test.c'
source_filename = "test.c"
target datalayout = "e-m:e-p270:32:32-p271:32:32-p272:64:64-i64:64-f80:128-n8:16:32:64-S128"
target triple = "x86_64-pc-linux-gnu"

@a = dso_local global i32 3, align 4
@b = dso_local global i32 5, align 4

; Function Attrs: noinline nounwind optnone uwtable
define dso_local void @reverse(i32 %0) #0 {
  %2 = alloca i32, align 4
  %3 = alloca i32, align 4
  store i32 %0, i32* %2, align 4
  %4 = load i32, i32* %2, align 4
  %5 = icmp sle i32 %4, 1
  br i1 %5, label %6, label %10

6:                                                ; preds = %1
  %7 = call i32 (...) @getint()
  store i32 %7, i32* %3, align 4
  %8 = load i32, i32* %3, align 4
  %9 = call i32 (i32, ...) bitcast (i32 (...)* @putint to i32 (i32, ...)*)(i32 %8)
  br label %16

10:                                               ; preds = %1
  %11 = call i32 (...) @getint()
  store i32 %11, i32* %3, align 4
  %12 = load i32, i32* %2, align 4
  %13 = sub nsw i32 %12, 1
  call void @reverse(i32 %13)
  %14 = load i32, i32* %3, align 4
  %15 = call i32 (i32, ...) bitcast (i32 (...)* @putint to i32 (i32, ...)*)(i32 %14)
  br label %16

16:                                               ; preds = %10, %6
  ret void
}

declare dso_local i32 @getint(...) #1

declare dso_local i32 @putint(...) #1

; Function Attrs: noinline nounwind optnone uwtable
define dso_local i32 @main() #0 {
  %1 = alloca i32, align 4
  %2 = alloca i32, align 4
  %3 = alloca i32, align 4
  %4 = alloca i32, align 4
  %5 = alloca i32, align 4
  %6 = alloca [4 x [2 x i32]], align 16
  %7 = alloca [4 x [2 x float]], align 16
  %8 = alloca i32, align 4
  %9 = alloca i32, align 4
  %10 = alloca i32, align 4
  %11 = alloca float, align 4
  %12 = alloca i32, align 4
  store i32 0, i32* %1, align 4
  %13 = call i32 (...) @_sysy_starttime()
  %14 = load i32, i32* @a, align 4
  %15 = load i32, i32* @b, align 4
  %16 = add nsw i32 %14, %15
  store i32 %16, i32* %2, align 4
  %17 = load i32, i32* %2, align 4
  %18 = call i32 (i32, ...) bitcast (i32 (...)* @putint to i32 (i32, ...)*)(i32 %17)
  store i32 5, i32* %5, align 4
  %19 = load i32, i32* %5, align 4
  %20 = load i32, i32* @b, align 4
  %21 = add nsw i32 %19, %20
  store i32 %21, i32* %3, align 4
  %22 = load i32, i32* %3, align 4
  %23 = call i32 (i32, ...) bitcast (i32 (...)* @putint to i32 (i32, ...)*)(i32 %22)
  %24 = bitcast [4 x [2 x i32]]* %6 to i8*
  call void @llvm.memset.p0i8.i64(i8* align 16 %24, i8 0, i64 32, i1 false)
  %25 = bitcast [4 x [2 x float]]* %7 to i8*
  call void @llvm.memset.p0i8.i64(i8* align 16 %25, i8 0, i64 32, i1 false)
  store i32 6, i32* %8, align 4
  %26 = load i32, i32* %8, align 4
  %27 = call i32 (i32, ...) bitcast (i32 (...)* @putint to i32 (i32, ...)*)(i32 %26)
  %28 = load i32, i32* %5, align 4
  %29 = icmp slt i32 %28, 0
  br i1 %29, label %30, label %31

30:                                               ; preds = %0
  store i32 1, i32* %1, align 4
  br label %66

31:                                               ; preds = %0
  %32 = load i32, i32* %5, align 4
  %33 = icmp eq i32 %32, 5
  br i1 %33, label %34, label %39

34:                                               ; preds = %31
  %35 = load i32, i32* @b, align 4
  %36 = icmp eq i32 %35, 10
  br i1 %36, label %37, label %38

37:                                               ; preds = %34
  store i32 25, i32* %5, align 4
  br label %38

38:                                               ; preds = %37, %34
  br label %42

39:                                               ; preds = %31
  %40 = load i32, i32* %5, align 4
  %41 = add nsw i32 %40, 15
  store i32 %41, i32* %5, align 4
  br label %42

42:                                               ; preds = %39, %38
  store i32 3, i32* %9, align 4
  br label %43

43:                                               ; preds = %46, %42
  %44 = load i32, i32* %9, align 4
  %45 = icmp slt i32 %44, 100
  br i1 %45, label %46, label %49

46:                                               ; preds = %43
  %47 = load i32, i32* %9, align 4
  %48 = add nsw i32 %47, 30
  store i32 %48, i32* %9, align 4
  br label %43

49:                                               ; preds = %43
  %50 = load i32, i32* %9, align 4
  %51 = call i32 (i32, ...) bitcast (i32 (...)* @putint to i32 (i32, ...)*)(i32 %50)
  store i32 1, i32* %10, align 4
  br label %52

52:                                               ; preds = %60, %49
  %53 = load i32, i32* %10, align 4
  %54 = icmp slt i32 %53, 5
  br i1 %54, label %55, label %63

55:                                               ; preds = %52
  store float 5.000000e+01, float* %11, align 4
  %56 = load float, float* %11, align 4
  %57 = load i32, i32* %10, align 4
  %58 = sitofp i32 %57 to float
  %59 = fdiv float %56, %58
  store float %59, float* %11, align 4
  br label %60

60:                                               ; preds = %55
  %61 = load i32, i32* %10, align 4
  %62 = add nsw i32 %61, 1
  store i32 %62, i32* %10, align 4
  br label %52

63:                                               ; preds = %52
  store i32 6, i32* %12, align 4
  %64 = load i32, i32* %12, align 4
  call void @reverse(i32 %64)
  %65 = call i32 (...) @_sysy_stoptime()
  store i32 0, i32* %1, align 4
  br label %66

66:                                               ; preds = %63, %30
  %67 = load i32, i32* %1, align 4
  ret i32 %67
}

declare dso_local i32 @_sysy_starttime(...) #1

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.memset.p0i8.i64(i8* nocapture writeonly, i8, i64, i1 immarg) #2

declare dso_local i32 @_sysy_stoptime(...) #1

attributes #0 = { noinline nounwind optnone uwtable "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="all" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+cx8,+fxsr,+mmx,+sse,+sse2,+x87" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="all" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+cx8,+fxsr,+mmx,+sse,+sse2,+x87" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #2 = { argmemonly nounwind willreturn }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 10.0.0-4ubuntu1 "}

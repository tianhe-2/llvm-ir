;a=3
@a=global i32 3, align 4
;b=5
@b=global i32 5, align 4

declare i32 @getint(...) #1
declare void @putint(...) #1
declare void @_sysy_starttime(...) #1
declare void @_sysy_stoptime(...) #1
declare void @putch(...) #1


define void @reverse (i32 %a) {
    ;int next
    %next = alloca i32, align 4          
    ;if n<=1
    %c = icmp sle i32 %a, 1
    br i1 %c, label %Ifequal, label %IfUnequal
Ifequal:
    ;next=getint()
    %in =call i32 (...) @getint()
    store i32 %in, i32* %next,align 4
    ;putout(next)
    %out=load i32,i32* %next,align 4
    call void (...) @putint(i32 %out)
    br label %end
IfUnequal:
    ;next=getint()
    %inun =call i32 (...) @getint()
    store i32 %inun, i32* %next,align 4
    ;reverse(n-1)
    %re= sub nsw i32 %a,1
    call void @reverse(i32 %re)
    ;putint(next)
    %outun=load i32,i32* %next,align 4
    call void (...) @putint(i32 %outun)
    br label %end
end:
    ret void

}

define i32 @main(){
    call void (...) @_sysy_starttime()     ;starttime()

    ;difference on domain of global var define and local define
    %1=alloca i32,align 4   ;int c
    %2=alloca i32,align 4   ;int d
    %3=alloca i32,align 4   ;int e
    %a=load i32,i32* @a,align 4  ;a(global)
    %b=load i32,i32* @b,align 4  ;b(global)
    %c=add i32 %a,%b        ;a(global)+b
    store i32 %c,i32* %1,align 4     ;c=a(global)+b
    call void (...) @putint( i32 %c)        ;putint(c)
    %local_a=alloca i32,align 4    ;int a(local)
    store i32 5,i32* %local_a,align 4      ;a(local)=5
    %local_a_value=load i32,i32* %local_a,align 4
    %result=add i32 %local_a_value,%b         ;a(local)+b
    store i32 %result,i32* %2,align 4     ;d=a(local)+b
    call void (...) @putint( i32 %result)        ;putint(d)


    ;array
    %4=alloca [4x[2x i32]]     ;arr (*here is just letter x)
    %5=alloca [4x[2x float]]   ;brr 
    
    ;implicit type conversion
    %6=alloca i32,align 4     ;int f
    store i32 6,i32* %6,align 4  ;f=6
    %f=load i32,i32* %6,align 4
    call void (...) @putint( i32 %f)        ;putint(f)

    ;if
    %if_a=icmp slt i32 %local_a_value,0      ;if(a<0)
    br i1 %if_a ,label %re_a, label %con_a
re_a:                            ;a<0 do
    ret i32 1
con_a:                            ;a>=0 do
    %if_a2=icmp eq i32 %local_a_value,5       ;if(a==5) 
    br i1 %if_a2 ,label %if_a2_eq,label %if_a2_une
if_a2_eq:                            ;a==5 do
    %if_b=icmp eq i32 %b,10           ;if(b==10)
    br i1 %if_b, label %if_b_eq, label %init   
if_b_eq:                                 ;b==10 do
    store i32 25,i32* %local_a,align 4      ;a=25
    ;store i32 25,i32 %local_a_value,align 4
    br label %init
if_a2_une:                            ;a!=5 do
    %a2=add i32 %local_a_value,15
    ;store i32 %a2,i32 %local_a_value,align 4
    store i32 %a2,i32* %local_a,align 4   ;a=a+15
    br label %init


init:
    ;while
    %i=alloca i32,align 4     ;int i
    store i32 3,i32* %i, align 4  ;i=3
    br label %while

while:
    %i_value=load i32,i32* %i,align 4
    %loop_jud=icmp slt i32 %i_value,100   ;i<100?
    br i1 %loop_jud, label %loop_while, label %while_end
loop_while:                            ;i<100
    %i_new_value=add i32 %i_value,30   ;i+30
    store i32 %i_new_value,i32* %i  ;i=i+30
    br label %while


while_end:                             ;i>=100
    %i_end=load i32,i32* %i,align 4
    call void (...) @putint( i32 %i_end)        ;putint(i)
    br label %for

for:
    ;for
    %for_i=alloca float,align 4       ;int i(within for block)  float i
    store float 1.0,float* %for_i  ;i=1
    br label %for_jud
for_jud:  
    %for_i_value=load float, float* %for_i                         ;check if i<5
    %judfor=fcmp olt float %for_i_value, 5.0
    br i1 %judfor,label %for_loop,label %for_out
for_loop:
    %n=alloca float,align 4     ;float n
    store float 50.0,float* %n    ;n=50 (implicit type conversion)
    %n_value=load float,float* %n
    %new_n=fdiv float %n_value, %for_i_value  ;n/i
    store float %new_n,float* %n             ;n=n/i
    %new_i=fadd float %for_i_value,1.0         ;i+1
    store float %new_i,float* %for_i            ;i=i+1
    br  label %for_jud


    ;reverse
for_out:
    %j = alloca i32, align 4      ;int j
    store i32 6,i32* %j,align 4  ;j=6
    %j_value=load i32,i32* %j,align 4
    call void @reverse (i32 %j_value)
    call void (...) @_sysy_stoptime()
    ret i32 0
}
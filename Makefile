.PHONY: pre, lexer, ast-gcc, ast-llvm, cfg, ir-gcc, ir-llvm, allopt,aopt,topt,uopt,asm, obj, exe, antiobj, antiexe,ir-s
pre:
	gcc test.c -E -o jiecheng.i
lexer:
	clang -E -Xclang -dump-tokens jiecheng.c > token.c 2>&1 

# 生成`main.c.003t.original`
ast-gcc:
	gcc -fdump-tree-original-raw jiecheng.c

# 生成`ast`
ast-llvm:
	clang -E -Xclang -ast-dump test.c

# 会生成多个阶段的文件 (.dot)，可以被 graphviz 可视化，可以直接使用 vscode 插件
# (Graphviz (dot) language support for Visual Studio Code)。
# 此时的可读性还很强。`main.c.011t.cfg.dot`
cfg:
	gcc -O0 -fdump-tree-all-graph jiecheng.c

#此时可读性不好，简要了解各阶段更迭过程即可。
ir-gcc:
	gcc -O0 -fdump-rtl-all-graph jiecheng.c

#生成ll文件
ir-llvm:
	clang -S -emit-llvm test.c

#根据源程序编译出的ll生成可执行文件
ir-yuan:
	clang test.ll sylib.c -o test
#ll链接sylib生成可执行文件
ir-s:
	clang myMain.ll sylib.c -o main

#全优化
allopt:
	llc -print-before-all -print-after-all jiecheng.ll >jiecheng.log 2>&1

asm:
	gcc -O0 -o main.S -S -masm=att jiecheng.i

obj:
	gcc -O0 -c -o main.o main.S

antiobj:
	objdump -d main.o > main-anti-obj.S
	nm main.o > main-nm-obj.txt
exe:
	gcc -O0 -o main main.o

antiexe:
	objdump -d main > main-anti-exe.S
	nm main > main-nm-exe.txt

clean:
	rm -rf *.c.*

clean-all:
	rm -rf *.c.* *.o *.S *.dot *.out *.txt *.ll *.i main
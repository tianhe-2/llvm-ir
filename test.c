int a = 3;
int b = 5;
void reverse(int n) {
    int next;
    if (n <= 1) {
        next = getint();
        putint(next);
    }
    else {
        next = getint();
        reverse(n - 1);
        putint(next);
    }
}
int main() {
    _sysy_starttime();
    //test global var define
    int c, d, e;
    c = a + b;
    putint(c);
    //test domain of global var define and local define
    int a = 5;
    d = a + b;
    putint(d);
    //array
    const int arr[4][2] = {};
    float brr[4][2] = {};
    //implicit type conversion
    int f = 6.5;
    putint(f);
    //if
    if (a < 0) {
        return 1;
    }
    if (a == 5) {
        if (b == 10)
            a = 25;
    }
    else
        a = a + 15;

    //while
    int i = 3;
    while (i < 100)
    {
        i += 30;
    }
    putint(i);

    //for
    for (int i = 1; i < 5; i++)
    {
        float n = 50;
        n = n / i;
    }
    //简单递归
    int j = 6;
    reverse(j);
    _sysy_stoptime();
    return 0;
}


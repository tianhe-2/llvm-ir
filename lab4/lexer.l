%option noyywrap
%{
    /*
    * You will need to comment this line in lab5.
    */
    #define ONLY_FOR_LEX
    
    #ifdef ONLY_FOR_LEX
    #else
    #define YYSTYPE void *
    #include "parser.h"
    #endif

    #define YY_NO_UNPUT
    #define YY_NO_INPUT
    #include <string>

    #ifdef ONLY_FOR_LEX
    #include <ostream>
    #include <fstream>
    #include <iomanip>
    using namespace std;
    extern FILE *yyin; 
    extern FILE *yyout;


    //YY_USER_ACTION 可以定义宏以提供始终在匹配规则的操作之前执行的操作
    //只要扫描程序匹配标记时，标记的文本就存储在以空字符终止的字符串yytext中，而且它的长度存储在yyleng中
    int isglobal=0;//判断是不是全局
    int tablenum=0;  //符号表里的符号总数
    int offset=0;
    int place=0; //这个是指我找到的符号，在符号表里的数组下标，主要是因为写在ID内部她报错，所以写成了全局变量
    int symbol_level=0;//作用域等级，0为最高，下一等级可以调用这一等级，现在还不管宏定义
    string now_type="";//记录变量的类型
    int isdeclaration=0;//记录是不是定义，在int 和float里置1  加入符号表后置0

    void DEBUG_FOR_LAB4(std::string s){
        std::string DEBUG_INFO = "[DEBUG LAB4]: \t" + s + "\n";
        fputs(DEBUG_INFO.c_str(), yyout);
    }

    void DEBUG_FOR_LAB4(std::string s, int row, int col){
        std::string DEBUG_INFO = "[DEBUG LAB4]: \t" + s + "\tline: " + std::to_string(row) + "\tcol: " + std::to_string(col) + "\n";
        fputs(DEBUG_INFO.c_str(), yyout);
    }
    #endif
    


    struct vartable
    {
        char* name;//变量名
        string type;//变量类型
    };
    class symbolnode{
        public:
            symbolnode*parent;
            //一个作用域下先写10个作用域
            symbolnode*child[10];
            struct vartable table_var[100];
            int sym_num;//符号个数
            int child_num;//孩子节点个数
            symbolnode(){
                parent=NULL;
                for(int i=0;i<10;i++)
                    child[i]=NULL;
                sym_num=0;
                child_num=1;
            }
            void setparent(symbolnode*node){
                parent=node;

            }
            void setchild(){
                child[child_num]=new symbolnode();
                child_num++;

            }
            //给小括号准备的
            void setfirstchild(){

                child[0]=new symbolnode();
                //child_num++;

            }
            void insert(vartable symbol){
                table_var[sym_num].name=(char*)malloc(50*sizeof(char));
                strcpy(table_var[sym_num].name,symbol.name);
                table_var[sym_num].type=symbol.type;
                sym_num++;
            }
            //变量名+类型判断
            //返回在符号表里的index
            int isin(char *name){
                // int flag=1
                //type，再次使用现在也没发判断用的那个，所以只用变量名判断
               // printf("%d\n",sym_num);
                if(sym_num==0){
                    return -1;
                }
               // printf("%s\n",name);
                for(int i=sym_num-1;i>=0;i--){
                 //   printf("%s\n",table_var[i].name);
                 //   printf("%d\n",strcmp(name,table_var[i].name));
                    int flag=0;
                    flag=strcmp(name,table_var[i].name);
                    if(!flag){
                    //    printf("place%d\n",i);
                        return i;
                    }
                }
                //int a=-1;
                //不存在的return不确定对不对
               // printf("fanhui%d\n",a);
                return -1;
            }


    };
    //是根节点，存全局变量
    symbolnode *nownode=new symbolnode();
    symbolnode *root=new symbolnode();
    #define YY_USER_ACTION         \
    offset += yyleng; \
   
    
    //有一个大括号则增加一个孩子节点，小括号开辟孩子节点，放在孩子节点的第一个，这个可以供大括号的变量用

    //struct vartable table_var[100];
    string dec2hex(long long int i) //将int转成16进制字符串
    {
        stringstream ioss; //定义字符串流
        string s_temp; //存放转化后字符
        ioss << setiosflags(ios::uppercase) << hex << i; //以十六制(大写)形式输出
        //ioss << resetiosflags(ios::uppercase) << hex << i; //以十六制(小写)形式输出//取消大写的设置
        ioss >> s_temp;
        return s_temp;
    }

%}

FLOAT ({DECIMIAL}\.[0-9]+|{DECIMIAL}\.[0-9]+[Ee](\+|-)?[0-9]+)
HEXADECIMAL (0[xX][0-9a-fA-F]+)
OCTAL (0[0-7]+)
DECIMIAL ([1-9][0-9]*|0)
ID [[:alpha:]_][[:alpha:][:digit:]_]*
EOL (\r\n|\n|\r)
WHITE [\t ]
LINECOMMENT \/\/[^\n]*{EOL}
BLOCKCOMMENTBEGIN "/*"
BLOCKCOMMENTELEMENT .
BLOCKLINE {EOL}
BLOCKCOMMENTEND "*/"
%x BLOCKCOMMENT

%%
"break" {
    #ifdef ONLY_FOR_LEX
        DEBUG_FOR_LAB4("BREAK\tbreak",yylineno,offset-yyleng);
    #else
        return BREAK;
    #endif 
}
"const" {
    #ifdef ONLY_FOR_LEX
        DEBUG_FOR_LAB4("CONST\tconst",yylineno,offset-yyleng);
    #else
        return CONST;
    #endif 
}
"continue" {
    #ifdef ONLY_FOR_LEX
        DEBUG_FOR_LAB4("CONTINUE\tcontinue", yylineno, offset - yyleng);
    #else
        return CONTINUE;
    #endif 
}
"for" {
    #ifdef ONLY_FOR_LEX
        DEBUG_FOR_LAB4("FOR\tfor", yylineno, offset - yyleng);
    #else
        return FOR;
    #endif 
}
"while" {
    #ifdef ONLY_FOR_LEX
        DEBUG_FOR_LAB4("WHILE\twhile", yylineno, offset - yyleng);
    #else
        return WHILE;
    #endif 
}

"int" {
    /*
    * Questions: 
    *   Q1: Why we need to return INT in further labs?
    *   Q2: What is "INT" actually?
    */
    #ifdef ONLY_FOR_LEX
        now_type="int";
        isdeclaration=1;
        DEBUG_FOR_LAB4("INT\tint",yylineno,offset-yyleng);
    #else
        return INT;
    #endif
}
"float" {
    /*
    * Questions: 
    *   Q1: Why we need to return INT in further labs?
    *   Q2: What is "INT" actually?
    */
    #ifdef ONLY_FOR_LEX
        now_type="float";
        isdeclaration=1;
        DEBUG_FOR_LAB4("INT\tint",yylineno,offset-yyleng);
    #else
        return INT;
    #endif
}
"void" {
    #ifdef ONLY_FOR_LEX
        now_type="float";
        isdeclaration=1;
        DEBUG_FOR_LAB4("VOID\tvoid",yylineno,offset-yyleng);
    #else
        return VOID;
    #endif 
}
"if" {
    #ifdef ONLY_FOR_LEX
        DEBUG_FOR_LAB4("IF\tif",yylineno,offset-yyleng);
    #else
        return IF;
    #endif
};
"else" {
    #ifdef ONLY_FOR_LEX
        DEBUG_FOR_LAB4("ELSE\telse",yylineno,offset-yyleng);
    #else
        return ELSE;
    #endif
};
"return" {
    #ifdef ONLY_FOR_LEX
        DEBUG_FOR_LAB4("RETURN\treturn",yylineno,offset-yyleng);
    #else
        return RETURN;
    #endif
}

"!" {
    #ifdef ONLY_FOR_LEX
        DEBUG_FOR_LAB4("LOGIC_NOT\t!", yylineno, offset - yyleng);
    #else
        return LOGIC_NOT;
    #endif
}
"&&" {
    #ifdef ONLY_FOR_LEX
        DEBUG_FOR_LAB4("LOGIC_AND\t!", yylineno, offset - yyleng);
    #else
        return LOGIC_AND;
    #endif
}
"||" {
    #ifdef ONLY_FOR_LEX
        DEBUG_FOR_LAB4("LOGIC_OR\t!", yylineno, offset - yyleng);
    #else
        return LOGIC_OR;
    #endif
}

"*" {
    #ifdef ONLY_FOR_LEX
        DEBUG_FOR_LAB4("PRODUCT\t*", yylineno, offset - yyleng);
    #else
        return PRODUCT;
    #endif
}
"/" {
    #ifdef ONLY_FOR_LEX
        DEBUG_FOR_LAB4("DIVISION\t/", yylineno, offset - yyleng);
    #else
        return DIVISION;
    #endif
}
"%" {
    #ifdef ONLY_FOR_LEX
        DEBUG_FOR_LAB4("REMAINDER\t%", yylineno, offset - yyleng);
    #else
        return REMAINDER;
    #endif
}
"~" {
    #ifdef ONLY_FOR_LEX
        DEBUG_FOR_LAB4("NOT\t~", yylineno, offset - yyleng);
    #else
        return NOT;
    #endif
}
"&" {
    #ifdef ONLY_FOR_LEX
        DEBUG_FOR_LAB4("AND\t&", yylineno, offset - yyleng);
    #else
        return AND;
    #endif
}
"|" {
    #ifdef ONLY_FOR_LEX
        DEBUG_FOR_LAB4("OR\t|", yylineno, offset - yyleng);
    #else
        return OR;
    #endif
}
"^" {
    #ifdef ONLY_FOR_LEX
        DEBUG_FOR_LAB4("XOR\t^", yylineno, offset - yyleng);
    #else
        return XOR;
    #endif
}

"+=" {
    #ifdef ONLY_FOR_LEX
        DEBUG_FOR_LAB4("ADDITION ASSIGNMENT\t+=", yylineno, offset - yyleng);
    #else
        return ADDITION_ASSIGNMENT;
    #endif
}
"-=" {
    #ifdef ONLY_FOR_LEX
        DEBUG_FOR_LAB4("SUBTRACTION ASSIGNMENT\t-=", yylineno, offset - yyleng);
    #else
        return SUBTRACTION_ASSIGNMENT;
    #endif
}
"*=" {
    #ifdef ONLY_FOR_LEX
        DEBUG_FOR_LAB4("MULTIPLICATION ASSIGNMENT\t*=", yylineno, offset - yyleng);
    #else
        return MULTIPLICATION_ASSIGNMENT;
    #endif
}
"/=" {
    #ifdef ONLY_FOR_LEX
        DEBUG_FOR_LAB4("DIVISION ASSIGNMENT\t/=", yylineno, offset - yyleng);
    #else
        return DIVISION_ASSIGNMENT;
    #endif
}
"%=" {
    #ifdef ONLY_FOR_LEX
        DEBUG_FOR_LAB4("MODULO ASSIGNMENT\t%=", yylineno, offset - yyleng);
    #else
        return MODULO_ASSIGNMENT;
    #endif
}

"==" {
    #ifdef ONLY_FOR_LEX
        DEBUG_FOR_LAB4("EQUAL TO\t==", yylineno, offset - yyleng);
    #else
        return EQUAL_TO;
    #endif
}
"!=" {
    #ifdef ONLY_FOR_LEX
        DEBUG_FOR_LAB4("NOT EQUAL TO\t!=", yylineno, offset - yyleng);
    #else
        return NOT_EQUAL_TO;
    #endif
}
"<=" {
    #ifdef ONLY_FOR_LEX
        DEBUG_FOR_LAB4("LESS EQUAL\t<=", yylineno, offset - yyleng);
    #else
        return LESS_EQUAL;
    #endif
}
">=" {
    #ifdef ONLY_FOR_LEX
        DEBUG_FOR_LAB4("GREATER EQUAL\t>=", yylineno, offset - yyleng);
    #else
        return GREATER_EQUAL;
    #endif
}
"=" {
    #ifdef ONLY_FOR_LEX
        DEBUG_FOR_LAB4("ASSIGN\t=",yylineno,offset-yyleng);
    #else
        return ASSIGN;
    #endif
}
"<" {
    #ifdef ONLY_FOR_LEX
        DEBUG_FOR_LAB4("LESS\t<",yylineno,offset-yyleng);
    #else
        return LESS;
    #endif
}
"+" {
    #ifdef ONLY_FOR_LEX
        DEBUG_FOR_LAB4("ADD\t+",yylineno,offset-yyleng);
    #else
        return ADD;
    #endif
}
";" {
    #ifdef ONLY_FOR_LEX
        DEBUG_FOR_LAB4("SEMICOLON\t;",yylineno,offset-yyleng);
    #else
        return SEMICOLON;
    #endif
}
"(" {
    #ifdef ONLY_FOR_LEX
       // symbol_level++;
        nownode->setfirstchild();
        symbolnode *newnode=new symbolnode();
        
        newnode=nownode->child[0];
        newnode->setparent(nownode);
        nownode=newnode;
        DEBUG_FOR_LAB4("LPAREN\t(",yylineno,offset-yyleng);
    #else
        return LPAREN;
    #endif
}
")" {
    #ifdef ONLY_FOR_LEX
        // symbol_level--;
        //调整节点
        nownode=nownode->parent;
        
        DEBUG_FOR_LAB4("RPAREN\t)",yylineno,offset-yyleng);
    #else
    return RPAREN;
    #endif
}
"[" {
    #ifdef ONLY_FOR_LEX
        DEBUG_FOR_LAB4("LSQUARE\t[", yylineno, offset - yyleng);
    #else
    return LSQUARE;
    #endif
}
"]" {
    #ifdef ONLY_FOR_LEX
        DEBUG_FOR_LAB4("RSQUARE\t]", yylineno, offset - yyleng);
    #else
    return RSQUARE;
    #endif
}
"{" {
    #ifdef ONLY_FOR_LEX
        //symbol_level+=2;
        symbolnode *newnode=new symbolnode();
        nownode->setchild();
        newnode=nownode->child[nownode->child_num-1];
        newnode->setparent(nownode);
        nownode=newnode;
        //printf("nownode   %d\n",nownode->sym_num);
        DEBUG_FOR_LAB4("LBRACE\t{",yylineno,offset-yyleng);
    #else
        return LBRACE;
    #endif
}
"}" {

    #ifdef ONLY_FOR_LEX
       // symbol_level-=2;
       nownode=nownode->parent;
        DEBUG_FOR_LAB4("RBRACE\t}",yylineno,offset-yyleng);
    #else
        return RBRACE;
    #endif
}

{ID} {
    int flag=0;
    symbolnode *searchnode=new symbolnode();
    // //search走，nownode会不会跟着走
    searchnode=nownode;
    // while(searchnode!=NULL){
    //     printf("%d\n",searchnode->sym_num);
    //     searchnode=searchnode->parent;
    // }
    //根节点判断
    


    //看看是不是要定义的
    if(isdeclaration==1){
        struct vartable newsym;
        newsym.type=now_type;
        newsym.name=(char*)malloc(50*sizeof(char));
        strcpy(newsym.name,yytext);
        //printf("%s",newsym.name);
        searchnode=nownode;
        nownode->table_var[nownode->sym_num].name=(char*)malloc(50*sizeof(char));
        strcpy(nownode->table_var[nownode->sym_num].name,yytext);
        nownode->table_var[nownode->sym_num].type=now_type;
        //nownode->table_var[nownode->sym_num]=newsym;
        nownode->sym_num++;
        // nownode->insert(newsym);
        place=nownode->sym_num-1;

        flag=1;

        isdeclaration=0;
    }
    //不是新定义的，就去树中找          
    else
    {
        while(searchnode!=NULL){
            if(searchnode==root){
                place=searchnode->isin(yytext);
                if(place>=0){
                flag=1;
                break;
                }
                else{
                    flag=0;
                    break;
                }

            }
            //在当前作用域找到了
           // printf("zhaoroot\n");
            place=searchnode->isin(yytext);
           // if(searchnode!=root)
             //   printf("palce%d shuliang%d\n",place,searchnode->parent->child[0]->sym_num!=0);
          //  printf("%d\n",place);
            if(place>=0){
                flag=1;
                break;
            }
            //去第一个孩子找找看有没,也就是小括号里面有没
            
            else if(searchnode->parent->child[0]->sym_num!=0){
             //   printf("zou2\n");
              //  printf("当前node所在域%s\n",searchnode->table_var[0].name);
                searchnode=searchnode->parent->child[0];
              //  printf("当前node所在域%s\n",searchnode->table_var[0].name);
                //string str="我要找了";
	           // printf("%s   %d\n",str.c_str(),searchnode->sym_num);
                place=searchnode->isin(yytext);
              //  printf("%d\n",place);
                //在第一个孩子里找到了
                if(place>=0){
                    flag=1;
                    break;
                }
                else
                    searchnode=searchnode->parent;   


            }

            //在当前作用域没找到
            else
                {
                    searchnode=searchnode->parent;  
                //    printf("孩子数量 %d 当前node符号表第一个 %s\n",searchnode->child_num,searchnode->table_var[0].name);
                 //   if(searchnode==NULL)
               //         printf("weikong");
                 //   printf("zou3\n");
                }  
            
        }
    
        // //在树中没找到
        if(flag==0){
            // struct vartable newsym;
            // newsym.type=now_type;
            // newsym.name=(char*)malloc(50*sizeof(char));
            // strcpy(newsym.name,yytext);
            // //printf("%s",newsym.name);
            // searchnode=nownode;
            // nownode->table_var[nownode->sym_num].name=(char*)malloc(50*sizeof(char));
            // strcpy(nownode->table_var[nownode->sym_num].name,yytext);
            // nownode->table_var[nownode->sym_num].type=now_type;
            // //nownode->table_var[nownode->sym_num]=newsym;
            // nownode->sym_num++;
            // // nownode->insert(newsym);
            // place=nownode->sym_num-1;
            //那说明没定义，就用了，这个输出没定义吧

        }
    }
   // printf("%s %d\n",yytext,flag);
    string address="";
    if(flag==1)
        address="0x"+dec2hex((long long int)(void*)searchnode->table_var[place].name);

    //printf("%s\n",address.c_str());
    #ifdef ONLY_FOR_LEX
        string s="";
        if(flag==1)
            s="ID\t"+string(yytext)+"\t"+to_string(yylineno)+"\t"+to_string(offset-yyleng)+"\t"+address;
        else
            s="ID\t"+string(yytext)+"\t"+to_string(yylineno)+"\t"+to_string(offset-yyleng)+"\t"+"未定义";

        DEBUG_FOR_LAB4(s);
    #else
        return ID;
    #endif
}
{FLOAT} {
        #ifdef ONLY_FOR_LEX
            
            DEBUG_FOR_LAB4("FLOAT\t"+std::string(yytext), yylineno, offset - yyleng);
        #else
            sscanf(yytext, "%f", &yylval.ftype);
            return FLOAT;
        #endif
    }
{DECIMIAL} {
        #ifdef ONLY_FOR_LEX
            DEBUG_FOR_LAB4("INTEGER\t"+std::string(yytext), yylineno, offset - yyleng);
        #else
            sscanf(yytext, "%d", &yylval.itype);
            return INTEGER;
        #endif
    }
{HEXADECIMAL} {
        #ifdef ONLY_FOR_LEX
            DEBUG_FOR_LAB4("INTEGER\t"+std::to_string(strtol(yytext,NULL,16)), yylineno, offset - yyleng);
        #else
            sscanf(std::to_string(strtol(yytext,NULL,16)).c_str(), "%d", &yylval.itype);
            return INTEGER;
        #endif
    }
{OCTAL} {
        #ifdef ONLY_FOR_LEX
            DEBUG_FOR_LAB4("INTEGER\t"+std::to_string(strtol(yytext,NULL,8)), yylineno, offset - yyleng);
        #else
            sscanf(std::to_string(strtol(yytext,NULL,8)).c_str(), "%d", &yylval.itype);
            return INTEGER;
        #endif
    }
{LINECOMMENT} {yylineno++; offset=0;}

{BLOCKCOMMENTBEGIN} {BEGIN BLOCKCOMMENT;}
<BLOCKCOMMENT>{BLOCKCOMMENTELEMENT} {}
<BLOCKCOMMENT>{BLOCKLINE}   {yylineno++;}
<BLOCKCOMMENT>{BLOCKCOMMENTEND}  {BEGIN INITIAL;}

{EOL} {yylineno++; offset=0;}
{WHITE}
%%

#ifdef ONLY_FOR_LEX
int main(int argc, char **argv){
     nownode=root;            
    if(argc != 5){
        fprintf(stderr, "Argument Not Enough");
        exit(EXIT_FAILURE);
    }

    if(!(yyin = fopen(argv[1], "r"))){
        fprintf(stderr, "No such file or directory: %s", argv[1]);
        exit(EXIT_FAILURE);
    }

    if(!(yyout = fopen(argv[3], "w"))){
        fprintf(stderr, "No such file or directory: %s", argv[3]);
        exit(EXIT_FAILURE);
    }

    yylex();
    return 0;
}
#endif